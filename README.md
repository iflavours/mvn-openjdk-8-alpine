Maven + OpenJDK 8 on Alpine Linux
===============================

This repository contains an Alpine Dockerized openjdk-8 + Maven, published to the public Docker Hub via automated build mechanism.

Usage
-----

Please notice this image is configured with a workdir `/code`, so make sure you mount such volume first.

### General usage

```bash
$ docker run -v $(pwd):/code iflavoursbv/mvn-openjdk-8-alpine:latest 'your-commands-here'
```

### You can run mvn commands like `mvn -v` simply:
```bash
$ docker run -ti --rm iflavoursbv/mvn-openjdk-8-alpine:latest mvn -v
```

### This image can be configured for holding the Maven cache artifacts:

```bash
$ docker run -ti --rm -v $(pwd):/code -v "$HOME/.m2":/root/.m2 iflavoursbv/mvn-openjdk-8-alpine:latest mvn clean compile
```

Configuration
-------------
This docker image is based on the following stack:
- OS: Alpine Linux
- Java: OpenJDK 8
- Maven: latest=3.3.9 (see [tag list](https://registry.hub.docker.com/u/iflavoursbv/mvn-openjdk-8-alpine/tags/manage/) for more versions)


Dependencies
------------
- [java:openjdk-8-jdk-alpine](https://hub.docker.com/r/library/java/tags/openjdk-8-jdk-alpine/)

History
-------
* 0.1.0 - Initial version

License
-------

[Licensed under MIT License](https://bitbucket.org/iflavours/mvn-openjdk-8-alpine/raw/master/LICENSE)

